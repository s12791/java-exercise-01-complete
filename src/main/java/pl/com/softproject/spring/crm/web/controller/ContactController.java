package pl.com.softproject.spring.crm.web.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.com.softproject.spring.crm.web.model.Contact;
import pl.com.softproject.spring.crm.web.dao.ContactDAO;
import pl.com.softproject.spring.crm.web.dao.CategoryDAO;

@Controller
@RequestMapping("/contact")
public class ContactController {

    @Autowired
    private ContactDAO contactDAO;

    @Autowired
    private CategoryDAO categoryDAO;

    @RequestMapping("/add")
    public ModelAndView addContact() {

        ModelAndView modelAndView = new ModelAndView("addContact");

        Contact contact = new Contact();
        modelAndView.addObject("contact", contact);
        modelAndView.addObject("categories", categoryDAO.findAll());

        return modelAndView;
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String saveContact(@Valid Contact contact, BindingResult binding) {

        if (binding.hasErrors()) {
            return "addContact";
        } else {
            System.out.println(contact);
            contactDAO.save(contact);
            return "redirect:/contact/listLogged.htm";
        }
    }

    @RequestMapping("/list")
    public ModelAndView list() {
        Iterable<Contact> res = contactDAO.findAll();

        ModelAndView model = new ModelAndView("notLoggedContactList");
        model.addObject("contacts", res);

        return model;
    }

    @RequestMapping("/listLogged")
    public ModelAndView listLogged() {
        Iterable<Contact> res = contactDAO.findAll();

        ModelAndView model = new ModelAndView("contactList");
        model.addObject("contacts", res);

        return model;
    }

    @RequestMapping("edit")
    public ModelAndView edit(@RequestParam int id) {

        ModelAndView model = new ModelAndView("addContact");

        Contact contact = contactDAO.findOne(id);
        model.addObject("contact", contact);
        model.addObject("categories", categoryDAO.findAll());

        return model;
    }

    @RequestMapping("delete")
    public String deleteContact(@RequestParam int id) {

        Contact contact = contactDAO.findOne(id);
        System.out.println(contact);

        contactDAO.delete(contact);
        return "redirect:/contact/listLogged.htm";

    }

}
