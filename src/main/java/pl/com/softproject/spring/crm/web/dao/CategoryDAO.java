package pl.com.softproject.spring.crm.web.dao;

import org.springframework.data.repository.CrudRepository;
import pl.com.softproject.spring.crm.web.model.Category;

public interface CategoryDAO extends CrudRepository<Category, Integer> {

}
