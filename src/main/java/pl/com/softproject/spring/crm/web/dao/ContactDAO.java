package pl.com.softproject.spring.crm.web.dao;

import org.springframework.data.repository.CrudRepository;
import pl.com.softproject.spring.crm.web.model.Contact;

public interface ContactDAO extends CrudRepository<Contact, Integer> {

    public Iterable<Contact> findByNameLike(String name);

}
