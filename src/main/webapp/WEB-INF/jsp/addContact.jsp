
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body> 
        <h1>Java - zadanie 1 :: <c:choose><c:when test="${not empty param['id']}">Edycja danych kontaktu</c:when><c:otherwise>Dodawanie kontaktu</c:otherwise></c:choose></h1>

        <form:form commandName="contact" method="POST" action="add.htm">

            <form:hidden path="id"/>
            <form:input path="name" placeholder="Imię" required="true" /><form:errors path="name"/><br/>
            <form:input path="lastName" placeholder="Nazwisko" required="true"/><form:errors path="lastName"/><br/>
            <form:input path="email" type="email" placeholder="Mail" required="true" /><form:errors path="email"/> <br/>
            <form:input path="password" placeholder="hasło" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" required="true" />przynajmniej jedna mała litera, duza litera, cyfra<form:errors path="password"/> <br/>
            <form:input path="mobile" type="tel" value ="111-111-111" pattern="\d{3}[\-]\d{3}[\-]\d{3}" placeholder="telefon" required="true" /><form:errors path="mobile"/><br/>
            <form:input path="birthDate" type="date" value = "05.03.2017" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" placeholder="data urodzenia" required="true"/><form:errors path="birthDate"/><br/>
            <form:select items="${categories}" path="category.id" itemValue="id" itemLabel="name"></form:select>




            <form:button>Zapisz</form:button>

            <c:choose><c:when test="${not empty param['id']}"><input type="button" onclick="location.href = 'http://localhost:8084/spring-crm-web/contact/delete.htm?id=${contact.id}';" value="usuń kontakt"/></c:when><c:otherwise></c:otherwise></c:choose>

        </form:form>

    </body>
</html>
