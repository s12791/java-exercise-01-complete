<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <script type="text/javascript">

            function toggleThis(id) {
                var tr = document.getElementById(id);
                if (tr) {
                    if (tr.style.display !== 'none') {
                        tr.style.display = 'none';
                    } else {
                        tr.style.display = '';
                    }
                }
            }
        </script>
    </head>
    <body>          

        <h1>Java - zadanie 1 :: Lista kontaktów</h1>
        <h3>Aby przejść do edycji/usuwania kliknij w id</h3>

        <table border="1">

            <th>ID</th>
            <th>Imię</th>
            <th>Nazwisko</th>
            <th>Mail</th>
            <th>Hasło</th>
            <th>Telefon</th>
            <th>Data urodzenia</th>
            <th>kategoria</th>
                <c:forEach items="${contacts}" var="contact" varStatus="loop">
                <tr>
                    <td colspan="8"><input type="button" value="+/-" onClick="toggleThis(${loop.index})"/> <b>${contact.name} ${contact.lastName}</b>
                    </td>
                </tr>

                <tr id="${loop.index}" style="display:none">

                    <td> <a href="<c:url value="/contact/edit.htm"/>?id=${contact.id}">${contact.id}</a></td>
                    <td>${contact.name}</td>
                    <td>${contact.lastName}</td>
                    <td>${contact.email}</td>
                    <td>${contact.password}</td>
                    <td>${contact.mobile}</td>
                    <td>${contact.birthDate}</td>
                    <td>${contact.category.name}</td>
                </tr>
            </c:forEach>
        </table>
        <p><a href="/spring-crm-web/contact/add.htm">Dodaj Kontakt</p>
        <p><a href="/spring-crm-web/contact/list.htm">wyloguj</p>   


    </body>
</html>